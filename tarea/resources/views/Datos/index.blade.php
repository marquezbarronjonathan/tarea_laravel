@extends('layouts.app')

@section('content')

<div class="text-center">
<h1>Datos</h1>
</div>
<div class="container">
<table class="table table-bordered">
<thead class="thead-dark">
<tr>
<th>ID</th>
<th>NOMBRE</th>
<th>APELLIDO PATERNO</th>
<th>APELLIDO MATERNO</th>
<th>FECHA NACIMIENTO</th>
</tr>

</thead>
<tbody>

@foreach($datos as $dato)
<tr>
<td>{{$dato->id}}</td>
<td>{{$dato->nombre}}</td>
<td>{{$dato->apellidop}}</td>
<td>{{$dato->apellidom}}</td>
<td>{{$dato->fechan}}</td>


</tr>
@endforeach

</tbody>
</table>
<a href="{{url('/Datos/create')}}" class="btn btn-primary">Agregar</a>
</div>

@endsection